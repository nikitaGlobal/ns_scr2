<?php
class SearchItems
{
    public $items;
    public function __construct($query)
    {
        $classname='search'.ScrTools::alfanum($query[4]);
        if (class_exists($classname)) {
            $search=new $classname();
            $this->items=$search->getItemsLinks($query);
        }
    }
}
foreach (scandir('sclasses/')as $file) {
    if (is_file('sclasses/'.$file)&& (strpos($file, '.php'))) {
        include('sclasses/'.$file);
    }
}
