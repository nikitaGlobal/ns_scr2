<?php
class ItemsFromXLSX
{
    public function __construct($file)
    {
        if ($xlsx=SimpleXLSX::parse($file)) {
            $this->rows=($xlsx->rows());
        } else {
            die('wrong XLSX file');
        }
    }
    public function getRows()
    {
        $out=array();
        foreach ($this->rows as $key=> $row) {
            if ($key<=1) {
                continue;
            }
            $out[$row[6]][]=$row;
        }
        array_multisort(array_map('count', $out), SORT_DESC, $out);
        $rows=array();
        foreach ($out as $vendor=>$items){
            $rows=array_merge($rows,$items);
        }
        return $rows;
    }
}
require_once('SimpleXLSX.php');