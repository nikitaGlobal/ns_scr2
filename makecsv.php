<?php
class makeCSV
{
    public function __construct()
    {
        $this->_makeCSV(
            $this->_makeCSVrows(
                $this->_filterRows(
                    $this->_populateRows(
                        $this->_transformRows(
                            $this->_processRows(
                                $this->_getValues()
                            )
                        )
                    )
                )
            )
        );
    }

    private function _makeCSV($rows)
    {
        $filename = 'import.csv';
        $file = dirname(__FILE__) . '/' . $filename;
        $fp = fopen($file, 'w');
        echo $file;
        foreach ($rows as $row) {
            fputcsv($fp, $row);
        }
        (fclose($fp));
        echo '<a href="' . SITEURL . '' . $filename . '">' . $filename . '</a>';
        echo "\n" . md5_file($file);
        echo "\n";
    }

    private function _filterRows($incomingRows)
    {
        $out = array();
        foreach ($incomingRows as $row) {
            $rowsWithParts[$row['partnumber']][] = $row;
        }
        foreach ($rowsWithParts as $part => $rows) {
            $first=true;
            foreach ($rows as $row) {
                if (strlen($row['Name'])==0) {
                    continue;
                }
                if ($first) {
                    $row['url']='## '.$row['url'];
                    $first=false;
                }
                $out[] = $row;
            }
        }
        return $out;
        $rowsWithParts = array();
        $out = array();
        foreach ($incomingRows as $row) {
        }
        foreach ($rowsWithParts as $part => $rows) {
            if (count($rows) == 1) {
                //        $out[] = $rows[0];
                //            continue;
            }
            $out[] = $this->_filterRowsPrice($this->_filterRowsEmpty($rows));
        }
        return $out;
    }

    private function _filterRowsEmpty($rows)
    {
        $out = array();
        foreach ($rows as $key => $value) {
            if (strlen($value['Name']) == 0) {
                print_r($value);
                continue;
            } else {
                $out[] = $value;
            }
        }
        return $out;
    }
    private function _filterRowsPrice($rows)
    {
        $out = array();
        usort($rows, function ($a, $b) {
            return strlen($a['Description']) < strlen($b['Description']);
        });
        return $rows;
    }

    private function _makeCSVrows($rows)
    {
        $keys = array_keys($rows[0]);
        $out = array($keys);
        foreach ($rows as $index => $row) {
            $tmpRow = array();
            foreach ($keys as $colnumber => $key) {
                $out[$index + 1][$colnumber] = $row[$key];
            }
        }
        return $out;
    }
    private function _populateRows($rows)
    {
        $defaults = array('post_status' => 'publish', 'visibility' => 'visible', 'stock_status' => 'instock', 'download' => 'no', 'virtual' => 'no');
        foreach ($rows as $key => $value) {
            $rows[$key] = array_merge($value, $defaults);
        }
        return $rows;
    }

    private function _transformRows($rows)
    {
        foreach ($rows as $key => $values) {
            foreach ($values as $col => $value) {
                $method = '_transformRows' . $col;
                if (method_exists($this, $method)) {
                    $rows[$key][$col] = $this->{$method}($value);
                }
            }
        }
        return $rows;
    }

    private function _transformRowsimages($values)
    {
        if (!strlen($values)) {
            return $values;
        }
        $images = explode(',', $values);
        if (empty($images)){
            return;
        }
        $out = array();
        foreach ($images as $image) {
            $basename = basename($image);
            $filepath = dirname(__FILE__) . '/images/' . $basename;
            if (!file_exists($filepath)) {
                echo "downloading " . $image . "\n";
                system('wget -c "'.$image.'" -O '.scrtools::alfanum($filepath).'.jpg');
                #file_put_contents($filepath, fopen($image, 'r'));
            }
            $out[] = SITEURL . 'images/' . urlencode($basename);
        }
        return implode(',', $out);
    }

    private function _processRows($rows)
    {
        $header = $this->_makeHeader($rows);
        $out = array();
        foreach ($rows as $key => $row) {
            foreach ($header as $col) {
                $out[$key][$col] = '';
            }
            foreach ($row as $k => $v) {
                $out[$key][$k] = $v;
                if (is_array($v)) {
                    $out[$key][$k] = implode(',', $v);
                }
            }
        }
        return $out;
    }

    private function _makeHeader($rows)
    {
        $out = array();
        foreach ($rows as $row) {
            $out = array_merge($out, array_keys($row));
        }
        return array_unique($out);
    }

    private function _getValues()
    {
        $q = 'SELECT * FROM transients WHERE `key` LIKE "item%"';
        $items = DB::getRows('SELECT * FROM transients WHERE `key` LIKE "item%"');
        $out = array();
        foreach ($items as $item) {
            $value = unserialize($item['value']);
            $out[] = $value;
        }
        return $out;
    }
}

require_once('transients.php');
require_once('abs.php');
new makeCSV();
