<?php
        require_once('scraperapi-php-sdk/src/Client.php');
require_once('transients.php');

abstract class ScrTools
{
    public static function path($content, $path)
    {
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        libxml_clear_errors();
        $doc->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
        $xpath = new DOMXpath($doc);
        $items = $xpath->query($path);
        if (!isset($items->length) || (int)$items->length < 1) {
            return false;
        }
        return $items;
    }
    public static function pathValue($content, $path)
    {
        $value = self::path($content, $path);
        if (!$value) {
            return false;
        }
        foreach ($value as $tag) {
            return $tag->nodeValue;
        }
    }
    public static function storeData($key, $data)
    {
        DB::setTransient($key, $data);
        return $key;
    }

    public static function getData($key)
    {
        return DB::getTransient($key);
    }

    public static function pathHtml($content, $path)
    {
        $items= self::path($content, $path);
        $out='';
        if (!$items) {
            return false;
        }
        foreach ($items as $item) {
            $out.=$item->ownerDocument->saveHTML($item);
        }
        return $out;
    }
    public static function pathValueId($content, $path)
    {
        return (self::pathValue($content, '//*[@id="'.$path.'"]'));
    }

    public static function tableClean($table)
    {
        $out=preg_replace('~<a [^>]*>(.*?)</a>~', '$1', $table);
        $out=preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si", '<$1$2>', $out);
        return $out;
    }
    public static function searchString($query, $delimiter=" ")
    {
        return urlencode(implode($delimiter, array($query[0],$query[1])));
    }

    public static function alfanum($s)
    {
        return preg_replace("/[^a-zA-Z0-9]+/", "", $s);
    }
    public static function prefix()
    {
        return 'scrape';
    }

    public static function sanitize($value)
    {
        return self::prefix() . md5(serialize($value));
    }

    public static function openPhantom($link)
    {
        $grabzIt = new \GrabzIt\GrabzItClient("MmM1MDYzNmJlNTI3NGRkNTk2MDVkZWVlYmQxY2IzNjQ=", "Pz8/RR9yRT8/HhUAGXY/SD9ISj8/Pz8/P0t5SG0/Pz8=");
    }
    public static function promptUrl($url)
    {
        $out=DB::getTransient('url'.self::sanitize($url));
        if ($out) {
            return $out;
        }
        echo('Please copy paste '."\n".$url."\n".' block from browser console, finish with [!!!]');
        $lines='';
        while (!strpos($lines, '[!!!]')) {
            $lines.=readline();
        }
        DB::setTransient('url'.self::sanitize($url, $lines));
        return $lines;
    }
    public static function extractJsVars($str)
    {
        preg_match_all('~^var\s+([^=]+?)\s*=\s*(.+?)\s*;?\s*$~imu', $str, $matchesAll, PREG_SET_ORDER);
        $arr = array();
        foreach ($matchesAll as $matches) {
            $arr[$matches[1]] = $matches[2];
        }
        return $arr;
    }
    public static function scraperAPI($url)
    {
        $out = DB::getTransient('urlscrape' . self::sanitize($url));
        echo 'OPENING URL ' . $url;
        echo "\n";
        if ($out || strlen($out) > 50) {
            echo "\n";
            echo "have in cache";
            echo "\n";
            return $out;
        }
        $client = new ScraperAPI\Client("8bab118334192bf392982d2c3cc35438");
        $result = $client->get($url, ["render"=>true])->raw_body;
        DB::setTransient('urlscrape' .self::sanitize($url), $result, 86400 * 4);
        return $result;
    }

    public static function getUrl($url)
    {
        $out = DB::getTransient('url' . self::sanitize($url));
        echo 'OPENING URL ' . $url;
        echo "\n";
        if ($out || strlen($out) > 50) {
            echo "\n";
            echo "have in cache";
            echo "\n";
            return $out;
        }
        $out=file_get_contents($url);
        DB::setTransient('url' .self::sanitize($url), $out, 86400 * 4);
        return $out;
    }
    public static function openUrl($url)
    {
        $out = DB::getTransient('url' . self::sanitize($url));
        echo 'OPENING URL ' . $url;
        if ($out || strlen($out) > 50) {
            echo "\n";
            echo "have in cache";
            echo "\n";
            return $out;
        }
        //        $url=str_replace('amazon.com//', 'amazon.com/', $url);
        echo "\n";
        echo 'OPENING URL ' . $url;
        echo "\n";
        $args = array(
            '-source',
            '--accept_all_cookies',
            '-cache=0',
            '--dump "' . $url . '"'
        );
        $cmd = 'lynx ' . implode(' ', $args);
        $out = shell_exec($cmd);
        if (strpos($out, 'id="captchacharacters"') || !strlen($out)) {
            echo 'Failed command ' . "\n";
            echo $cmd . "\n";
            echo('lynx --accept_all_cookies -cache=0 -image_links -verbose "' . $url . '"');
            die();
        }
        DB::setTransient('url' .self::sanitize($url), $out, 86400 * 4);
        return $out;
    }
}
