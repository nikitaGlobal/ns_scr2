<?php
class getItem
{
    public $item;
    public function __construct($query)
    {
        $this->items=array();
        $this->query=$query;
        $this->vendor=$query[0];
        $this->partnumber=$query[1];
        $this->description=$query[2];
        $this->price=$query[4];
        $this->site=$query[6];
        $this->noclass=false;
        $this->requiredFields=array('Name','Short Description','Description', 'Price','Images');
        $this->_initSearchClass();
        if ($this->noclass) {
            die();
        }
        $this->_getItems(
            $this->_searchItems($query)
        );
        if (is_array($this->items)&&!empty($this->items)) {
            foreach ($this->item as $key=> $item) {
                DB::setTransient('item'.$key, $item);
            }
        }
    }

    private function _initSearchClass()
    {
        foreach (scandir('sclasses/')as $file) {
            if (is_file('sclasses/'.$file)&& (strpos($file, '.php'))) {
                include_once('sclasses/'.$file);
            }
        }
        $classname='search'.ScrTools::alfanum($this->site);
        if (class_exists($classname)) {
            $this->search=new $classname();
            return;
        }
        $this->noclass=true;
        echo('No search class for '.$this->site.' ('.$classname.')');
    }

    private function _searchItems()
    {
        return $this->search->getItemsLinks($this->query);
    }

    private function _getItems($links)
    {
        if (!array($links)||empty($links)) {
            return $this->_itemNotFound();
        }
        foreach ($links as $link) {
            $key=$this->vendor.' '.$this->partnumber;
            $this->item[$key]=array_merge(
                array(
                    'url'=>$link,
                    'Partnumber'=>$this->partnumber,
                    'Vendor'=>$this->vendor
                ),
                $this->_collectItemdata($link)
            );
        }
    }

    private function _collectItemdata($link)
    {
        $out=array();
        foreach ($this->requiredFields as $field) {
            $methodName='itemGet'.Scrtools::alfanum($field);
            if (!method_exists($this->search, $methodName)) {
                print_r($out);
                die($this->site.' class has no method to fetch '.$field.' ('.$methodName.')');
            }
            $openmethod='openUrl';
            if (isset($this->search->openMethod)) {
                $openmethod=$this->search->openMethod;
            }
            $result=$this->search->{$methodName}(ScrTools::{$openmethod}($link));
            if (!$result&&!is_array($result)) {
                print_r($out);
                die('could not get '.$field.' from '.$link);
            }
            $out[$field]=$result;
        }
        return $out;
    }

    private function _itemNotFound()
    {
    }
}
//require_once('searchitems.php');
require_once('abs.php');
require_once('transients.php');
require_once('itemslist.php');
$readXLS=new ItemsFromXLSX('items.xlsx');
$count=0;
foreach ($readXLS->getRows() as $row) {
    if (in_array($row[6], array('anixter.com','surveillance-video.com'))) {
               $row[6]='amazon.com';
    }
    if (in_array($row[6],array('anixter.com'))) {
        $row[6]='digikey.com';
    }
    //tiger.com is down
    //arconweld.com has no search and not googlable
    //ingersollrand.com no available items
//twowayradio.com,motortronics,northerntool missing items,
//
    //t    $skip=array("mouser.com",'arconweld.com','ingersollrand.com','benshaw.com','anixter.com','barix.com','datapro.net','eis-inc.com','grainger.com','jameco.com','motortronics.com');
    $skip=array(
     //   'jameco.com',
        'twowayradio.com','ingesollrand.com','benshaw.com','tigerdirect.com','motortronics.com','northerntool.com');
    if (in_array($row[6], $skip)) {
       continue;
    }
    if ($row[6]!='mouser.com') {
        //continue;
    }
    $manual=array('jameco.com','datapro.net','eis-inc.com', 'grainger.com','samlexamerica.com','tessco.com','mouser.com');
    if (in_array($row[6],$manual)) {
        $row[6]='manual';
    }
    $item=new getItem($row);
    if (is_array($item->item)) {
        $count++;
    }
    echo "\n$count\n";
}
//new getItem();
