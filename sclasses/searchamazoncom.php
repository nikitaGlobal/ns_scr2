<?php
class searchamazoncom
{
    public function __construct()
    {
        $this->url='http://amazon.com';
        $this->searchurl='https://www.amazon.com/s?k=';
    }

    public function getItemsLinks($query)
    {
        $searchpage=scrtools::openUrl($this->searchurl.scrtools::searchstring($query));
        $items=Scrtools::pathValue($searchpage, '//div[@data-component-type="s-search-result"]//a[contains(@class,"a-text-normal")][1]//@href');
        if ($items) {
            return array($this->url.$items);
        }
    }
    public function itemGetName($itemcontent)
    {
        return trim(scrtools::pathValueId($itemcontent, "productTitle"));
    }

    public function itemGetPrice($itemcontent)
    {
        $price = SCRTools::pathValue($itemcontent, '//span[@id="priceblock_ourprice"]');
        if (!$price) {
            return " ";
        }
        return preg_replace('/[^0-9\.,]/', '', $price);
    }
    public function itemGetImages($itemcontent)
    {
        $imagesScript = SCRTools::pathValue($itemcontent, '//script[contains(.,"thumb") and contains(.,"colorImages")]');
        preg_match('/var data \= (.*)A\.trigger/ms', $imagesScript, $matches);
        if (!isset($matches[1])) {
            return array();
        }
        preg_match_all('/hiRes":"([^\"]+)\"/', $matches[1], $images);
        if (!isset($images[1])) {
            return array();
        }
        return ($images[1]);
    }

    public function itemGetDescription($itemcontent)
    {
        return $this->itemGetShortDescription($itemcontent);
    }
    public function itemGetShortDescription($itemcontent)
    {
        $desc=trim(scrtools::pathValueId($itemcontent, "productDescription"));
        if(!$desc) {
            return $this->itemGetName($itemcontent);
        }
        return $desc;
    }
}
