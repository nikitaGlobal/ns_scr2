<?php
class searchaceindustriescom
{
    public function __construct()
    {
        $this->url='aceindustries.com';
        $this->searchurl='https://www.aceindustries.com/search.aspx?searchterm=';
    }
    public function getItemsLinks($query)
    {
        $searchpage=scrtools::openUrl($this->searchurl.urlencode($query[1]));
        if (strpos($searchpage, 'Your search did not result in any matches')) {
            return false;
        }
        $link=(SCRTools::pathValue($searchpage, '//div[@id="product-row"]//a//@href'));

        if ($link) {
            return array(
                $this->url.$link
            );
        }
    }
    public function itemGetName($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//div[@class="product-info-wrap"]//h1');
    }
    public function itemGetShortDescription($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//meta[@name="description"]//@content');
    }

    public function itemGetDescription($itemcontent)
    {
        return $this->itemGetShortDescription($itemcontent);
    }
    public function itemGetPrice($itemcontent)
    {
        return (float)str_replace('$', '', scrtools::pathValue($itemcontent, '//meta[@itemprop="price"]//@content'));
    }
    public function itemGetImages($itemcontent)
    {
        $imagesblock=scrtools::pathHtml($itemcontent, '//div[@class="image-wrap product-image-wrap"]//script');
        if (!$imagesblock) {
            return array();
        }
        preg_match_all("/'(\/images\/product\/large[^']*)/",  $imagesblock, $matches);
        $out=array();
        foreach ($matches[1] as $image){
            $out[]=$this->url.$image;
        }
        return $out;
    }
}
