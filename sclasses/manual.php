<?php
class SearchManual
{
    public $openMethod;
    public function __construct()
    {
        $this->openMethod='getData';
    }

    public function getItemsLinks($query)
    {
        $key=$query[0].' '.$query[1];
        $results=array(
            'DATAPRO 1854-15C'=>array(
                "name"=>'Antenna Cable SMA to N-Type',
                'shortdescription'=>"The 1854 series is a wireless antenna extension cable, typically used to place an 802.11 antenna at a distance from it's hardware.",
                'description'=>"The 1854 series is a wireless antenna extension cable, typically used to place an 802.11 antenna at a distance from it's hardware.

For example, if you have a workstation with a PCI wireless card, but get poor WiFi reception; or if you host a network in your lobby, but have trouble reaching to the farthest corners; this cable will allow you to separate the broadcast antenna from the card or router it's attached to, and place it where you'd like.

The 1854 series features a standard-polarity SMA male connector on one end and a male N-Type connector on the other. SMA is a small connection typically found on commercial-grade routers and extenders; N-Type is a large connection typically found on Omni/Yagi antennas and larger equipment.

It's built from RG-58 coaxial wire for shielding and signal strength, and has a nominal impedance of 50 Ohms. This is the ideal type of wire for wireless signals, which is a curious thing to say but it makes  sense if you think about it.",
'price'=>35,
'images'=>array('https://www.datapro.net/images/1854_large.jpg')
            ),
        'MARBAL 6500-S10'=>array(
            'name'=>'Mar-Bal Glastic® Standoff Insulator, 13500 V, 3/8-16 x 5/8", 5" H',
            'shortdescription'=>'All Mar-Bal standoff insulators are molded from a flame retardant and track-resistant glass reinforced thermoset polyester molding compound recognized for current carrying devices under UL Material recognition number e 80533 (n).',
            'description'=>'Brand name :  Marbal
Trade Name :  Glastic®
Series :  Glastic® STAOFF-63-STL
Item Name :  Standoff Insulator
Short Desc :  STANDOFF,6500,13.5kv,5.0"H,3/8X16X5/8ST
Packaging :  20/Carton
UNSPSC :  39121721
They combine high mechanical strength with high arc resistance and dielectric properties at elevated temperatures and humidity. These shatter-resistant insulators meet the most exacting standards for a variety of applications.',
'price'=>38.12,
'images'=>array('https://www.eis-inc.com/medias/sys_master/h26/ha1/8800197115934.jpg')

        ),
        'SAMLEX PST-30S-24-A'=>array(
            'name'=>'PST-30S-24E',
            'shortdescription'=>'This highly efficient DC-AC inverter converts 24 Volts DC to 300 Watts of pure sine wave AC power at 230 Volts, 50 Hz. The unit comes with separate cigarette lighter plug cables and battery clamp cables. Features include overload protection, low battery alarm / shut down, low idle power draw of less than 0.5 Amps. AC power is available from a single standard VDE European outlet, accessible on the front panel.',
            'description'=>'<h2>Description</h2>

<table border="0">
<tbody>
<tr>
<td>Input Voltage</td>
<td>21 to 33 VDC</td>
</tr>
<tr>
<td>Output Voltage</td>
<td>230 VAC +/-3%</td>
</tr>
<tr>
<td>Dimensions</td>
<td>210 x 146 x 65 mm</td>
</tr>
<tr>
<td>Output</td>
<td>300 Watts</td>
</tr>
<tr>
<td>Fuse Rating</td>
<td>20 A x 1</td>
</tr>
<tr>
<td>Weight</td>
<td>1.5 Kgs</td>
</tr>
</tbody>
</table>
        
        <h2>Product benefits</h2>
         <ul>
<li>Pure sine wave output voltage
</li><li>High efficiency
</li><li>Switch-mode design
</li><li>230 VAC output from 24 VDC input
</li><li>LED indicators for power and protections
</li><li>Overload protection
</li><li>Protections: input low voltage, input over voltage, over temperature, over load, short circuit
</li><li>Minimal idle power consumption of less than 0.5 Amps
</li><li>Comes with 2 DC input wire sets – with battery clamps and with 12V plug
</li><li>2 year warranty</li></ul>

        <h2>Product documentation</h2>
         <table><tbody><tr><td><b>Language</b></td><td><b>File</b></td><td><b>Type</b></td></tr>
<tr><td>n.a.</td><td><a target="_blank" href="https://www.samlex.com/wp-content/uploads/PST-30S-24E.jpg">PST-30S-24E.jpg</a></td><td>Image</td></tr>
<tr><td><img src="/wp-content/uploads/en.png" border="0" title="English"></td><td><a target="_blank" href="https://www.samlex.com/wp-content/uploads/PST-30S-24E_slicksheet.pdf">PST-30S-24E_slicksheet.pdf</a></td><td>Specifications</td></tr>
<tr><td><img src="/wp-content/uploads/en.png" border="0" title="English"></td><td><a target="_blank" href="https://www.samlex.com/wp-content/uploads/Manual-PST-15S_30S_60S.pdf">Manual-PST-15S_30S_60S.pdf</a></td><td>Manual</td></tr>
<tr><td><img src="/wp-content/uploads/nl.png" border="0" title="Dutch"></td><td><a target="_blank" href="https://www.samlex.com/wp-content/uploads/Gebruiksaanwijzing--PST-15S_30S_60S.pdf">Gebruiksaanwijzing–PST-15S_30S_60S.pdf</a></td><td>Manual</td></tr>
<tr><td><img src="/wp-content/uploads/fr.png" border="0" title="French"></td><td><a target="_blank" href="https://www.samlex.com/wp-content/uploads/Manuel-PST-15S_30S-Fr.pdf">Manuel-PST-15S_30S-Fr.pdf</a></td><td>Manual</td></tr>
</tbody></table>',
'price'=>" ", 'images'=>array(
    'https://www.samlex.com/wp-content/uploads/PST-30S-24E.jpg'
)
        )
        );
        $results['SCHNEIDER 6KYC8']=array(
'name'=>'IEC Style Overload Relay, 48 to 65A, 3 Poles, Manual Reset, Trip Class: 10',
'shortdescription'=>'Looking for SCHNEIDER ELECTRIC IEC Style Overload Relay, 48 to 65A, 3 Poles, Manual Reset, Trip Class: 10 (6KYC8)? Grainger&#039;s got your back. Price $138.00. Easy online ordering for the ones who get it done along with 24/7 customer service, free technical support &amp; more. ',
'description'=>'					TeSys™ IEC Contactors and Overload Relays<br>For industrial motor control applications, combine a contactor with an overload relay to make a complete motor starter. Rated 600VAC. Saddle clamp terminals. DIN rail• or panel-mount.<br>UL Listed, CSA and CE Certified.<br>Overload Relays<br>For 3-phase applications. Bimetallic elements. Class 10 trip characteristics with 1-phase sensitivity. Ambient compensated. Adjustable current range. Lockable, sealable, manual/auto reset, test trip. Mount to underside of contactor.
',
'price'=>138,
'images'=>array('https://static.grainger.com/rp/s/is/image/Grainger/6KYC8_AS02?$zmmain$','https://static.grainger.com/rp/s/is/image/Grainger/6KYC8_AS02?$zmmain$')
        );
        $results['TESSCO 380507']=array(
            'name'=>'800-2500MHz 10dBi Directional Yagi Antenna',
            'shortdescription'=>'Digital Antenna 489-DB multi-band 800 - 2500 MHz high performance directional antenna can be used for all cellular frequencies. This antenna features a powerful multi-band radiator with 10 dBi gain allowing for focused power to capture more signals. The antenna includes integrated brackets to mount on a pole and has 6 inch of low loss 240 style cable terminated with an N female connector.&lt;br&gt;&lt;br&gt;Designed for easy outside installation on a mast or pole, the 489-DB multi-band directional antenna is ideal for use outside homes, offices and commercial buildings in areas of low cellular signal strength. This yagi antenna offers excellent signal pulling power for fixed installations where previously few solutions existed.',
'description'=>'Digital Antenna 489-DB multi-band 800 - 2500 MHz high performance directional antenna can be used for all cellular frequencies. This antenna features a powerful multi-band radiator with 10 dBi gain allowing for focused power to capture more signals. The antenna includes integrated brackets to mount on a pole and has 6 inch of low loss 240 style cable terminated with an N female connector.

Designed for easy outside installation on a mast or pole, the 489-DB multi-band directional antenna is ideal for use outside homes, offices and commercial buildings in areas of low cellular signal strength. This yagi antenna offers excellent signal pulling power for fixed installations where previously few solutions existed.<ul class="unlisted">
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Item Length
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
18 in                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Maximum VSWR
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
1.5:1                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Jumper Included
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
Yes                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Item Height
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
2.5 in                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Item Width
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
8.5 in                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Lightning Protection
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
DC Ground                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Manufacturer
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
Digital Antenna                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Horizontal Beamwidth
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
80 deg                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Maximum Rated Wind Velocity
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
100 mile/h                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Tuned Frequency (MHz)
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
N/A                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Item Weight
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
2.6 lb                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Qty/ Uom
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
1 Each                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Maximum Power
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
100 W                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Gain dBd
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
7.9 dBd                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Type of Hardware Included
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
U-Bolts                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Vertical Beamwidth
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
60 deg                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Specific Frequency
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
800-2500 MHz                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Polarization
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
Vertical                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                RF Connectors
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
N Female                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Gain dBi
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
10 dBi                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Type of Jumper Included
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
6" Low Loss DA240                            </div>
                        </li>
                        <li class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                Connector Placement
                            </div>
                            <div class="col-xs-6 col-sm-8 col-md-9">
Pigtail                            </div>
                        </li>
                </ul>',
                'price'=>321.84,
                'images'=>array('https://avalanche.tessco.com/productimages/450x450/279185.jpg')

        );
        $results['VELLEMAN 316654']=array(
            'name'=>'Analog Voltage Panel Meter 30 Volt DC2.8 X 2.4',
            'shortdescription'=>'Buy Analog Voltage Panel Meter 30 Volt DC2.8 X 2.4',
            'description'=>'<b>Analog DC Voltage Panel Meter, 30V (AVM7030)</b><br><br>Velleman analog panel meters are great for use in electronic instruments, switchboards, test bench panels, vehicles, etc. Each panel meter features moving coil indication and a zero adjustment screw.<br><br><b>Specifications:</b><ul><li>Range: 0 - 30V</li><li>Dimensions: 2.8 x 2.4 x 1.4 (W"xH"xD")</li><li>Class: 2.5</li></ul>',
            'price'=>14.95,
            'images'=>array(
                'https://www.jameco.com/Jameco/Products/MakeImag/316654.jpg'
            )
        );
        $results['LAIRD CONNECTIVITY YE240015']=array(
            'name'=>'YE240015',
            'shortdescription'=>'YE240015 Laird Connectivity Antennas YAGI,EC,15,2.4-2.5GH z datasheet, inventory & pricing',
            'description'=>'YE240015 Laird Connectivity Antennas YAGI,EC,15,2.4-2.5GH z datasheet, inventory & pricing.',
            'price'=>133,
            'images'=>array('https://eu.mouser.com/images/lairdtechnologies/lrg/YE240015_SPL.jpg')
        );
        $results['SIMPSON METER 0 2552']=array(
            'name'=>'529-02552',
            'shortdescription'=>'Analogue Panel Meters 1227 0-200 DCA 2.',
            'description'=>'Analogue Panel Meters 1227 0-200 DCA 2.',
            'price'=>123.16,
            'images'=>array('https://eu.mouser.com/images/simpson/lrg/Wide_Vue_analog_DSL.jpg')
        );
        if (isset($results[$key])) {
            return array(ScrTools::storedata($key.$query[6], $results[$key]));
        }
        var_dump($key);
        print_r($query);
        die('new item');
    }

    public function itemGetName($data)
    {
        return $data['name'];
    }
    public function itemGetShortDescription($data)
    {
        return $data['shortdescription'];
    }
    public function itemGetDescription($data)
    {
        return $data['description'];
    }
    public function itemGetPrice($data)
    {
        return $data['price'];
    }
    public function itemGetImages($data)
    {
        return $data['images'];
    }
}
