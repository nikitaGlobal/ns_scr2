<?php
class searchautomationdirectcom
{
    public $openMethod;
    public function __construct()
    {
        $this->openMethod ='scraperAPI';
        $this->url='https://www.automationdirect.com/';
        $this->searchurl='https://www.automationdirect.com/adc/search/search?fctype=adc.falcon.search.SearchFormCtrl&cmd=Search&categoryId=0&TxnNumber=-1&searchqty=10&searchquery=';
    }
    public function getItemsLinks($query)
    {
        $searchpage=scrtools::scraperAPI($this->searchurl.urlencode($query[1]));
        $link=scrtools::pathValue($searchpage, '//*[@id="prod_search_table"]//a[1]//@href');
        return array($this->url.$link);
    }

    public function itemGetName($itemcontent)
    {
        return trim(scrtools::pathValue($itemcontent, '//h1[@itemprop="name"]'));
    }
    public function itemGetDescription($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//*[@itemprop="description"]');
    }
    public function itemGetShortDescription($itemcontent){
        return scrtools::pathValue($itemcontent,'//meta[@name="description"]//@content');
    }
    public function itemGetPrice($itemcontent){
        return scrtools::pathValue($itemcontent,'//meta[@itemprop="price"]//@content');
    }
    public function itemGetImages($itemcontent){
        $images=scrtools::path($itemcontent, '//div[@class="sliderimage"]//img');
        if (!$images) {
            return array();
        }
        $out=array();
        foreach ($images as $image){
            $out[]=$image->getAttribute('src');
        }
        return $out;
    }
}
