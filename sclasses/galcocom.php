<?php
class SearchGalcocom
{
    public function __construct()
    {
        $this->url='https://galco.com';
        $this->searchurl='https://galco.com/scripts/cgiip.exe/wa/wcat/catalog.htm?searchbox=';
    }
    public function getItemsLinks($query)
    {
        $searchpage=ScrTools::openUrl($this->searchurl.ScrTools::searchString($query));
        $items=ScrTools::path($searchpage, '//div[@id="resultsdiv" and contains(@data-partnum,'.$query[1].')]//div[contains(@class,"productName")]//a');
        if (!isset($items->length) ||(int)$items->length<0) {
            return false;
        }
        return $this->_itemsLinksProcess($items);
    }

    public function getItem($link)
    {
        $itemcontent=ScrTools::openUrl($link);
    }
    private function _itemsLinksProcess($items)
    {
        $out=array();
        foreach ($items as $item) {
            $out[]=$this->url.($item->getAttribute('href'));
            break;
        }
        return $out;
    }

    public function itemGetName($itemcontent)
    {
        return ScrTools::pathValueId($itemcontent, 'hdr1text').' '.ScrTools::pathValueId($itemcontent, 'hdr2text');
    }

    public function itemGetShortDescription($itemcontent)
    {
        return ScrTools::pathValue($itemcontent, '//meta[@name="description"]//@content');
    }
  
    public function _ItemGetImagesVertical($itemcontent)
    {
        if (strpos(ScrTools::pathhtml($itemcontent,'//img[@id="displayImageVertical"]'),'NOPICTURE')) {
            return array();
        }
        if (strpos($itemcontent, 'productImageXLG')) {
            return array($this->url.ScrTools::pathValue($itemcontent, '//img[@id="displayImageVertical"]//@src'));
        }
    }

    public function itemGetImages($itemcontent)
    {
        $out=array();
        $images=ScrTools::path($itemcontent, '//*[@id="imgCarousel1"]//img//@data-image');
        if (!isset($images->length)|| $images->length==0) {
            return $this->_ItemGetImagesVertical($itemcontent);
        }
        foreach ($images as $image) {
            if ($this->url.'/'.$image->nodeValue=='https://galco.com/images/catalog/picture-na_md.jpg')
            {
                return array();
            }
            $out[]=$this->url.'/'.$image->nodeValue;
        }
        return $out;
    }
    
    public function itemGetPrice($itemcontent)
    {
        if (ScrTools::pathValue($itemcontent, '//div[contains(@class,"requestquote")]')) {
            return " ";
        }
        $price=ScrTools::pathValueId($itemcontent, "displayPriceValue");
        if (!$price) {
            return " ";
        }
        return $price;
    }
    
    public function itemGetDescription($itemcontent)
    {
        $description=ScrTools::pathHtml($itemcontent, '//*[@itemprop="description"]//div//*');
        if (!$description){
            return $this->itemgetshortDescription($itemcontent);
        }
        return $description;
    }
}
