<?php
class searchalliedeleccom
{
    public $openMethod;
    public function __construct()
    {
        $this->url='https://www.alliedelec.com';
        $this->searchurl='https://www.alliedelec.com/view/search?keyword=';
        $this->openMethod='getUrl';
    }

    public function getItemsLinks($query)
    {
        $searchpage=scrtools::getUrl($this->searchurl.urlencode($query[1]));
        $links=scrtools::pathValue($searchpage, '//*[contains(@class,"search-result-description-title")][1]//a[1]/@href');
        return array($this->url.$links);
    }

    public function itemGetName($itemcontent)
    {
        return scrtools::pathvalue($itemcontent, '//h1[contains(@class,"product-display-name")]');
    }

    public function itemGetShortDescription($itemcontent)
    {
        return scrtools::pathvalue($itemcontent, '//div[contains(@class,"description")]//p[2]');
    }
    public function itemGetDescription($itemcontent)
    {
        $description=scrtools::pathhtml($itemcontent, '//div[contains(@class,"exp-coll-mobile-content")]//*');
        if (!$description) {
            return $this->itemgetshortDescription($itemcontent);
        }
        return $description;
    }

    public function itemGetPrice($itemcontent)
    {
        if (strpos($itemcontent, "Product Discontinued. Replacement available.")) {
            return " ";
        }
        return (float)scrtools::pathValue($itemcontent, '//p[contains(@class,"standard-pricing-price")]//@price');
    }

    public function itemGetImages($itemcontent)
    {
        $out=array();
        $images=scrtools::path($itemcontent, '//*[@id="main-image-gallery"]//img');
        foreach ($images as $image) {
            $out[]=$image->getAttribute('src');
        }
        return $out;
    }
}
