<?php
class searchfiberopticcableshopcom
{
    public $openMethod;
    public function __construct()
    {
        $this->openMethod='scraperAPI';
        $this->url='https://www.fiberopticcableshop.com/';
        $this->searchurl='https://www.fiberopticcableshop.com/nsearch.html?catalog=fiberopticcables&searchsubmit.x=0&searchsubmit.y=0&.autodone=nsearch.&query=';
    }

    public function getItemsLinks($query)
    {
        $searchpage=scrtools::scraperapi($this->searchurl.urlencode($query[1]));
        $link=scrtools::pathValue($searchpage, '//*[@id="results"]//a[contains(@class,"link")]//@href');
        return array($link);
    }
    public function itemGetShortDescription($itemcontent)
    {
        return $this->itemGetName($itemcontent);
    }

    public function itemGetDescription($itemcontent)
    {
        return $this->itemGetName($itemcontent);
    }

    public function itemGetName($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//*[contains(@class,"main-contents")]//h1');
    }

    public function itemGetImages($itemcontent)
    {
        $crumbs=$this->url.scrtools::pathValue($itemcontent, '//table[contains(@class,"main-contents")]//div[@style="width:700px"]//a[last()]//@href');
        $prevpage=scrtools::scraperAPI($crumbs);
        $image=scrtools::pathValue($prevpage, '//table[contains(@class,"main-contents")]//a[@rel="lightbox[product]"]//@href');
        if ($image) {
            return array($image);
        }
        return array();
//        $image=scrtools::pathValue($prevpage,'//table[contains(@class,"main-contents")]//img[@alt="Click to enlarge"]');
    }

    public function itemGetPrice($itemcontent)
    {
        $script=scrtools::pathValue($itemcontent, '//script[contains(.,"listPrice")]');
        if ($script) {
            preg_match("/listPrice': '([^']*)'/", $script, $matches);
            print_r($matches);
            return $matches[1];
        }
    }
}
