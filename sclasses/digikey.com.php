<?php class Searchdigikeycom
{
    public function __construct()
    {
        $this->url='https://www.digikey.com/';
        $this->searchurl='https://www.digikey.com/products/en?keywords=';
    }

    public function getItemsLinks($query)
    {
        $searchpage=scrtools::openUrl($this->searchurl.urlencode($query[1]));
        if (!ScrTools::pathValueId($searchpage, 'custom-search') && !ScrTools::pathValueId($searchpage,'search-within-results')) {
            return array($this->searchurl.scrtools::searchString($query));
        }
        if (strpos($searchpage,'productTable')|| strpos($searchpage,'Sort Table')) {
            $item=ScrTools::pathValue($searchpage,'//table[@id="productTable"]//tbody//tr[1]//td[@class="tr-image"]//a//@href');
            if ($item) {
                return $this->url.$item;
            }
        }
        print_r($query);
        die();
    }

    public function itemGetName($itemcontent)
    {
        return trim(scrtools::pathValue($itemcontent, '//*[@itemprop="model"]'));
    }

    public function itemGetDescription($itemcontent)
    {
        return Scrtools::tableClean(ScrTools::pathHtml($itemcontent, '//table[@id="product-overview"]'));
    }

    public function itemGetShortDescription($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//*[@itemprop="description"]');
    }
    public function itemGetPrice($itemcontent)
    {
        if (strpos(ScrTools::pathhtml($itemcontent, '//*[contains(@class, "product-details-headline")]'),'Non-Stock')){
            var_dump(123);
           return " "; 
        }
        return (float)scrtools::pathValue($itemcontent, '//*[@itemprop="price"]');
    }

    public function itemGetImages($itemcontent)
    {
        if (strpos($itemcontent, 'product-photo-carousel')) {
            $image=ScrTools::path($itemcontent, '//div[@class contains "product-photo-carousel"]//img//@src');
            print_r($images);
            die();
        }
        if (strpos(ScrTools::pathHtml($itemcontent,'//*[@id="product-photo"]'),'Photo Not Available')) {
            return array();
        }
        die();
    }
}
