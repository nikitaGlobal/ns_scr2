<?php
class searchlcomcom
{
    public $openMethod;
    public function __construct()
    {
        $this->openMethod="scraperAPI";
        $this->url='https://www.l-com.com';
        $this->searchurl='https://www.l-com.com/search?view_type=grid&keywords=';
    }

    public function getItemsLinks($query)
    {
        $searchpage =scrtools::scraperAPI($this->searchurl.urlencode($query[1]));
        if (!strpos($searchpage, "did not match any products.") && strpos($searchpage, "section-wrap description-block")) {
            return array($this->searchurl.urlencode($query[1]));
        }
    }
    public function itemGetName($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//h1[@itemprop="name"]');
    }
    public function itemGetShortDescription($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//meta[@name="description"]//@content');
    }
    public function itemGetImages($itemcontent)
    {
        $links=scrtools::pathHtml($itemcontent, '//div[@class="thumb-container"]//a');
        preg_match_all("/largeimage: '([^']*)'/", $links, $matches);
        $out=array();
        foreach ($matches[1] as $link){
            $out[]=$this->url.$link;
        }
        return $out;
    }
    public function itemGetPrice($itemcontent)
    {
        return (float)str_replace('Price: $', '', scrtools::pathValue($itemcontent, '//span[@class="actual-price"]'));
    }
    public function itemGetDescription($itemcontent)
    {
        return scrtools::pathhtml($itemcontent, '//*[@class="full-description"]');
    }
}
