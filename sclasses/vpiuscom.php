<?php
class searchvpius
{
    public function __construct()
    {
        $this->openMethod='scraperAPI';
        $this->url='https://www.vpi.us';
        $this->urlsearch="https://www.vpi.us/search?q=";
    }

    public function getItemsLinks($query)
    {
        $searchpage=SCRTools::scraperAPI($this->urlsearch.urlencode($query[1]));
        if (SCRTools::pathValue($searchpage, '//*[@id="right-product-text"]//h1')) {
            return array($this->urlsearch.urlencode($query[1]));
        }
    }

    public function itemgetimages($itemcontent)
    {
        $images=scrtools::path($itemcontent,'//*[@id="image-block"]//img//@src');
        if (!$images) {
            return array();
        }
        $out=array();
        foreach ($images as $image){
            $out[]=$image->nodeValue;
        }
        return $out;
    }

    public function itemGetPrice($itemcontent)
    {
        $prices=scrtools::path($itemcontent, '//*[@itemprop="price"]');
        $array=array();
        if (!$prices) {
            return " ";
        }
        foreach ($prices as $price) {
            $pricelist[]=(float)$price->nodeValue;
        }
        return (float)array_sum($pricelist)/count($pricelist);
    }

    public function itemGetDescription($itemcontent)
    {
        return SCRTools::pathhtml($itemcontent, '//*[@id="idTab1"]//*');
    }
    public function itemGetName($itemcontent)
    {
        return SCRTools::pathValue($itemcontent, '//*[@id="right-product-text"]//h1');
    }
    public function itemGetShortDescription($itemcontent)
    {
        return SCRTools::pathValue($itemcontent, '//meta[@name="description"]//@content');
    }
}
