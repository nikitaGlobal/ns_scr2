<?php
class searchastrondistributorscom
{
    public function __construct()
    {
        $this->openMethod ='scraperAPI';
        $this->url='https://www.astrondistributors.com/';
        $this->searchurl='https://www.astrondistributors.com/search/results.html?search_in_description&main_page=advanced_search_result&keyword=';
        $this->searrchurl='https://www.astrondistributors.com/search/results.html?keyword=';
    }

    public function getItemsLinks($query)
    {
        $results=array('LS-18A'=>
            'https://www.astrondistributors.com/astron-ls-18a-4127.html',
             'LS-10A'=>'https://www.astrondistributors.com/astron-ls-10a-4126.html',
        );
        if (!isset($results[$query[1]])) {
            return false;
            die('NO RESULT FOR '.$query[1]);
        }
        return array($results[$query[1]]);
    }
    public function itemGetName($itemcontent)
    {
        return scrtools::pathvalueId($itemcontent, 'productName');
    }
    public function itemGetShortDescription($itemcontent)
    {
        return scrtools::pathvalue($itemcontent, '//meta[@name="description"]//@content');
    }

    public function itemGetDescription($itemcontent)
    {
        $table=scrtools::pathhtml($itemcontent, '//*[@id="productDescription"]//table');
        return scrtools::tableClean($table);
    }

    public function itemGetPrice($itemcontent)
    {
        return ((float)str_replace('$','',scrtools::pathValue($itemcontent, '//span[@id="productPrices"]//span')));
    }
    public function itemGetImages($itemcontent){
        $image=ScrTools::pathValue($itemcontent,'//[@id="mainProductImage"]//@href');
        if (!$image) {
            return " ";
        }
        return $this->url.$image;
    }

}
