<?php
class Searchsuperbreakersnet
{
    public function __construct()
    {
        $this->url='https://www.superbreakers.net/';
        $this->searchurl='https://www.superbreakers.net/catalogsearch/result/?q=';
    }

    public function getItemsLinks($query)
    {
        print_r($query);
        $string=str_replace(' ', '+', (implode('+', array($query[0], $query[1]))));
        $searchpage=ScrTools::openUrl($this->searchurl.$string);
        $items=ScrTools::pathValue($searchpage, '//div[@class="category-products"]//ul//li[1]//h2[@class="product-name"]//a//@href');
        if (!$items) {
            print_r($query);
            die();
        }
        return array($items);
    }

    public function itemGetName($itemcontent)
    {
        $name=ScrTools::pathValue($itemcontent, '//*[@itemprop="name"]/h1');
        if ($name) {
            return trim($name);
        }
    }

    public function itemGetImages($itemcontent)
    {
        return array(ScrTools::pathValue($itemcontent,'//img[@id="image"]//@src'));
    }

    public function itemGetPrice($itemcontent)
    {
        return (float)ScrTools::pathValue($itemcontent, '//*[@itemprop="price"]//@content');
    }

    public function itemGetDescription($itemcontent)
    {
        $description=$this->itemGetShortDescription($itemcontent);
        $description.=ScrTools::pathHtml($itemcontent, '//*[@itemprop="description"]//p[@class="indent"]');
        return $description;
    }
    public function itemGetShortDescription($itemcontent)
    {
        $description =ScrTools::pathValue($itemcontent, '//*[@itemprop="description"]//h2');
        if ($description) {
            return $description;
        }
    }
}
