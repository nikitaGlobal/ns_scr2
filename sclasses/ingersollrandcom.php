<?php
class Searchingersollrandcom
{
    public function __construct()
    {
        $this->url='https://ingersollrand.com';
        $this->searchurl='https://www.ingersollrand.com/en-us/search.html?query=';
    }
    public function getItemsLinks($query)
    {
        $searchpage=SCRTools::openUrl($this->searchurl.urlencode($query[1]));
        if (!strpos($searchpage, '<div id="noResults"')) {
            die('have results');
        }
    }
}
