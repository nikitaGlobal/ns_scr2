<?php
class searchhuntcctvcom
{
    public $openMethod;
    public function __construct()
    {
        $this->url='https://huntcctv.com';
        $this->searchurl='https://huntcctv.com/search/?search=';
        $this->openMethod='scraperAPI';
    }
    public function getItemsLinks($query)
    {
        $searchpage=ScrTools::scraperAPI($this->searchurl.urlencode($query[1]));
        $link=scrtools::pathValue($searchpage, '//*[@id="content"]//h4//..//..//a//@href');
        if ($link) {
            return array($this->url.$link);
        }
    }

    public function itemGetName($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//meta[@property="og:title"]//@content');
    }

    public function itemGetDescription($itemcontent)
    {
        return scrtools::pathhtml($itemcontent, '//*[@id="product-description"]');
    }

    public function itemGetShortDescription($itemcontent)
    {
        return scrtools::pathValue($itemcontent, '//meta[@property="og:description"]//@content');
    }
    public function itemGetImages($itemcontent)
    {
        $images=scrtools::path($itemcontent, '//figure[@class="woocommerce-product-gallery__wrapper"]//a');
        if (!$images) {
            return false;
        }
        $out=array();
        foreach ($images as $image){
            $out[]=$image->getAttribute('href');
        }
        return $out;
    }
    public function itemGetPrice($itemcontent)
    {
        return " ";
    }
}
