<?php
class searchbarixcom
{
    public function __construct()
    {
        $this->url='https://www.barix.com';
    }

    public function getItemsLinks($query)
    {
        $results=array(
            "BARIONET100"=>"https://www.barix.com/devices/barionet/barionet-100/", "ANNUNCICOM100"=>"https://www.barix.com/devices/annuncicom/annuncicom-100/"
        );
        if (!isset($results[$query[1]])) {
            return false;
        }
        return array($results[$query[1]]);
    }
    
    public function itemGetName($itemcontent)
    {
        return scrtools::pathvalue($itemcontent, '//meta[@property="og:title"]//@content');
    }
    public function itemGetShortDescription($itemcontent)
    {
        return scrtools::pathvalue($itemcontent, '//h3[@class="subhead"]');
    }
    public function itemGetDescription($itemcontent)
    {
        $classes=array("product-description", "main-features","technical-specs");
        $out='';
        foreach ($classes as $class) {
            $out.=scrtools::tableclean(scrtools::pathhtml($itemcontent, '//div[contains(@class,"'.$class.'")]'));
        }
        return $out;
    }

    public function itemGetPrice($itemcontent)
    {
        return " ";
    }

    public function itemGetImages($itemcontent)
    {
        $images=scrtools::path($itemcontent, '//div[@class="product-images"]//img');
        if (!$images) {
            return array();
        }
        $out=array();
        foreach ($images as $image) {
            $out[]=$image->getAttribute('src');
        }
        return $out;
    }
}
