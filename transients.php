<?php
class DBCore
{
    public function __construct()
    {
        $this->conn   = new mysqli(
            DB_HOST,
            DB_USER,
            DB_PASS,
            DB_BASE
        );
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        if (file_exists('.install')) {
            $this->_installDB();
            die('DB installed');
        }
        $this->_suffixOld = '(UNIX_TIMESTAMP(time)+expires<UNIX_TIMESTAMP());';
    }
    private function _installDB()
    {
        $id     = '`id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`),';
        $tables = array(
                'users'      => 'chatid INT UNIQUE, ' .
                                '`info` TEXT,' .
                                '`regions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin',
                'transients' => '`key` varchar(100) UNIQUE, `value` LONGTEXT,
                `expires` INT DEFAULT 10800,
                `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
                'queue'      => 'chatid INT, sum varchar(32), status BOOLEAN DEFAULT false',
                'subscriptions'=>'chatid INT, districtid INT',
                'options'    => '
                `key` varchar(100) UNIQUE, `value` LONGTEXT',
                'messages'   => '
                `sum` varchar(32) UNIQUE,
                `districtid` int NOT NULL,
                `text` LONGTEXT,
                `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                ',
                'districts'  => ' `id` INT NOT NULL AUTO_INCREMENT ,
                `city` VARCHAR(200) NOT NULL ,
                `district` VARCHAR(200) NOT NULL , PRIMARY KEY (`id`)'
            
            );
        foreach ($tables as $table => $cols) {
            ($this->query(
                'CREATE TABLE IF NOT EXISTS `' . $table . '` (' . $cols . ') ' .
                    'ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;'
            ));
        }
        $this->query('ALTER TABLE `districts` ADD UNIQUE (`city`, `district`);');
        $this->query('ALTER TABLE `queue` ADD UNIQUE (`chatid`, `sum`);');
        $this->query('ALTER TABLE `subscriptions` ADD UNIQUE (`chatid`, `districtid`);');
        unlink('.install');
    }
        
        
    public function updateOption($args)
    {
        $value = addslashes($args[1]);
        if (! is_string($args[1])) {
            $value = (serialize($args[1]));
        }
        $values  = (
            '"' . $args[0] . '","' . addslashes($value) . '"'
        );
        $values2 = 'value="' . addslashes($value) . '"';
            
        return $this->query(
            'INSERT INTO options (`key`, `value`) VALUES (' . $values . ') ON DUPLICATE KEY UPDATE ' . $values2 . ';'
        );
    }
        
    public function saveRow($args)
    {
        $table  = $args[0];
        $row    = $args[1];
        $keys   = $this->implodeWrap(array_keys($row), '`');
        $values = $this->implodeWrap($row);
        $query  = 'INSERT INTO `' . $table . '` (' . $keys . ') VALUES (' . $values . ')';
        if (isset($args[2])) {
            unset($row[$args[2]]);
            $moreValues = array();
            foreach ($row as $k => $v) {
                $moreValues[] = (
                    '`' . $k . '`="' . addslashes($v) . '"'
                );
            }
            $query .= ' ON DUPLICATE KEY UPDATE ' . implode(',', $moreValues);
        }
        $query .= ";";
            
        return $this->query($query);
    }
        
    public function getOption($key)
    {
        if (is_array($key)) {
            $key=$key[0];
        }
        $value = $this->querySelect('SELECT value FROM options WHERE `key`="' . addslashes($key) . '";');
        if (! $value) {
            return false;
        }
            
        $result = @unserialize($value['value']);
        if ($result === false) {
            return $value['value'];
        }
            
        return unserialize($value['value']);
    }
    public function setTransient($args)
    {
        $this->_deleteOldTransients();
        @$value = addslashes($args[1]);
        if (! is_string($args[1])) {
            $serialize = serialize($args[1]);
            $value     = addslashes($serialize);
        }
        if (! isset($args[2])) {
            $args[2] = 10800;
        }
        $values  = (
            '"' . $args[0] . '","' . $value . '", ' . (int)$args[2]
        );
        $values2 = 'value="' . $value . '"';
        if ($this->getTransient($args[0])) {
            $this->deleteTransient($args[0]);
        }
        $this->query(
            'DELETE FROM `transients` WHERE `key`="'.$args[0].'" LIMIT 1'
        );
        $query = 'INSERT IGNORE INTO transients (`key`, `value`, `expires`) VALUES (' .
                     $values . ')';
            
        return $this->query(
            $query
        );
    }
    public function deleteTransient($key)
    {
        return $this->query('DELETE FROM `transients` WHERE `key`="' . addslashes($key) . '" LIMIT 1');
    }
        
    public function getTransient($key)
    {
        if (is_array($key)) {
            $key=$key[0];
        }
        $this->_deleteOldTransients();
        $q=  'SELECT value FROM transients WHERE `key`="'
                . addslashes($key) . '";';
        $value = $this->querySelect(
            $q
        );
        if (! $value) {
            return false;
        }
        @$result = unserialize($value['value']);
        if ($result === false) {
            return $value['value'];
        }
            
        return unserialize($value['value']);
    }
        
    public function implodeWrap($values, $wrap = '"')
    {
        foreach ($values as $key => $value) {
            $values[$key] = addslashes($value);
        }
            
        return $wrap . implode($wrap . ', ' . $wrap, ($values)) . $wrap;
    }
        
    public function querySelect($query)
    {
        $result = $this->query($query);
        switch ($result->num_rows) {
                case 0:
                    return false;
                    break;
                case 1:
                    return $result->fetch_assoc();
                    break;
                default:
                    return $result->fetch_assoc();
            }
    }
        
    public function getRows($query)
    {
        $result = $this->query($query);
        $rows   = array();
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
            
        return $rows;
    }
        
    public function query($query)
    {
        $result = $this->conn->query($query);
        if ($this->conn->errno) {
            die('DB Error: ' . mysqli_error($this->conn) . ', query: <br>' . "\n" . $query);
        }
        return $result;
    }
        
    private function _deleteOldTransients()
    {
	return;
        $query = 'DELETE FROM `transients` WHERE ' . $this->_suffixOld;
        $this->query($query);
    }
}

abstract class DB
{
    private static function _initDB()
    {
        return new DBCore();
    }

    public static function __callStatic($method, $args)
    {
        if (is_array($args) && count($args)==1) {
            $args=$args[0];
        }
        $db=self::_initDB();
        return $db->{$method}($args);
    }
}
require_once('settings.php');
